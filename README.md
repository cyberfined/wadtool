#Wadtool
It is util to edit archive, which looks like a doom wad.
[Read more about archive structure](https://bitbucket.org/Undefined3102/wad_lib)

#Building
```bash
git clone https://Undefined3102@bitbucket.org/Undefined3102/wadtool.git
cd wadtool
mkdir build
cd build
cmake ..
cmake --build .
cd ../bin
./wadtool
```
#Requirments
* CMake >= 3.5
* GTK2
* C compiler

#License
GPLv3. Read LICENSE

#Author
undefined3102@gmail.com
